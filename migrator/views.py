from django.shortcuts import render, get_object_or_404, render_to_response
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from migrator.forms import LocalEndpointForm, RemoteEndpointForm
from migrator.models import IMAPEndpoint, Migration
from django.conf import settings
from django.utils.translation import ugettext as _

# Create your views here.

def index(request):
    return HttpResponseRedirect(reverse('migrator:localEndpoint'))


def local_endpoint(request):
    #Test if the form was sent
    if request.method=="POST":
        form = LocalEndpointForm(request.POST)
        if form.is_valid():
            imap_object=form.save(commit=False)
            imap_object.host_imap=settings.DEFAULT_IMAP_SERVER
            if imap_object.connection():
                imap_object.save()
                request.session['local_endpoint_id']=imap_object.id
                return HttpResponseRedirect(reverse('migrator:remoteEndpoint'))
    else:
        form = LocalEndpointForm()

    return render(request, 'migrator/localendpoint.html', {'form':form})


def remote_endpoint(request):
        #Test if the form was sent
        if request.method=="POST":
            form = RemoteEndpointForm(request.POST)
            if form.is_valid():
                remote_imap=form.save(commit=False)
                if remote_imap.connection():
                    remote_imap_list=remote_imap.connection().list()
                    remote_imap.save()
                    if 'local_endpoint_id' in request.session:
                        local_endpoint_id = request.session['local_endpoint_id']
                        local_imap= IMAPEndpoint.objects.get(id=local_endpoint_id)
                        migration= Migration(origin = local_imap, target = remote_imap)
                        migration.save()
                        return render(request, 'migrator/recapIMAP.html', {'migration': migration, 'remote_imap_list': remote_imap_list})
        else:
            form = RemoteEndpointForm()

        return render(request, 'migrator/localendpoint.html', {'form':form})

def parse_list_response(line):
    list_response_pattern = re.compile(r'\((?P<flags>.*?)\) "(?P<delimiter>.*)" (?P<name>.*)')
    flags, delimiter, mailbox_name = list_response_pattern.match(line).groups()
    mailbox_name = mailbox_name.strip('"')
    return (flags, delimiter, mailbox_name)

def recap_imap(request):
    list_response_pattern = re.compile(r'\((?P<flags>.*?)\) "(?P<delimiter>.*)" (?P<name>.*)')
    parse_list_response(list_response_pattern)
    return render(request, 'migrator/recapIMAP.html', {'Flags': flags, 'Delimiter': delimiter, 'Mailbox Name': mailbox_name})


def text_message(request):
        def __init__(self, text):
            self.lines = text.split('\015\012')
            self.lines.reverse()

        def readline(self):
            try:
                return self.lines.pop() + '\n'
            except:
                return ''


def affiche_emails(request, imap):
        imap.connection().select('INBOX')
        status, uids = imap.uid('search')
        if status != 'OK':
            return HttpResponseRedirect('Impossible de récupérer les infos')
        uids = uids[0].split(' ')
        uids.reverse()
        uids = uids[:5]

        for id in uids:
            if id.strip() == '':
                continue
            status, res = imap.uid('fetch', id, '(BODY.PEEK[HEADER.FIELDS(From Subject)])')
            if status != 'OK':
                return HttpResponseRedirect('Impossible de lire le mail n°%s' %id)
            else:
                message = Message(textMessage(res[0][1]),0)
                sujet = decodeHeader(message['sublect'])
                de = decodeHeader(message['from'])
                return render(request, 'migrator/afficheEmails.html', {'Message': message, 'Sujet': sujet, 'De': de})

        imap.close()