# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('migrator', '0012_auto_20150701_1637'),
    ]

    operations = [
        migrations.AlterField(
            model_name='migration',
            name='processing_date',
            field=models.DateTimeField(default='Not yet started', verbose_name='Start Date', null=True),
        ),
    ]
