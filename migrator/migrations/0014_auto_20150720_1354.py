# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('migrator', '0013_auto_20150720_1353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='migration',
            name='processing_date',
            field=models.DateTimeField(verbose_name='Start Date', null=True),
        ),
    ]
